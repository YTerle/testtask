package com.example.yura.albums;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.yura.albums.Album.Album;
import com.example.yura.albums.Album.AlbumActivity;
import com.example.yura.albums.Album.AlbumAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView albumsListView;
    AlbumAdapter albumsListViewAdapter;

    private List<Album> albums;
    private String albumsUrl = "http://jsonplaceholder.typicode.com/albums";

    private static final String TAG_ID = "id";
    private static final String TAG_TITLE = "title";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InternetAccessChecker internetChecker = new InternetAccessChecker(this);
        internetChecker.trackInternetConnection();

        albumsListView = (ListView)findViewById(R.id.listAlbums);
        albums = new ArrayList<>();

        new AlbumsDownloader().execute();
    }

    private class AlbumsDownloader extends AsyncTask<Void, Void, Void> {

        private String retrieveJSONString(String url){
            String JSONOutputStr = null;
            try {
                URL albumsUrl = new URL(url);
                HttpURLConnection AlbumsConnection = (HttpURLConnection) albumsUrl.openConnection();
                AlbumsConnection.setRequestMethod("GET");
                AlbumsConnection.connect();

                InputStream in = AlbumsConnection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String lineOfJSONCode;
                while ((lineOfJSONCode = streamReader.readLine()) != null)
                    responseStrBuilder.append(lineOfJSONCode);

                JSONOutputStr = responseStrBuilder.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return JSONOutputStr;
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            String JSONOutputStr = retrieveJSONString(albumsUrl);

            if (JSONOutputStr != null) {
                try {

                    JSONArray jsonArray = new JSONArray(JSONOutputStr);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String id = jsonObject.getString(TAG_ID);
                        String title = jsonObject.getString(TAG_TITLE);

                        albums.add(new Album(id, title));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("parser", "Couldn't get any data from the url");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            albumsListViewAdapter = new AlbumAdapter(MainActivity.this, albums);
            albumsListView.setAdapter(albumsListViewAdapter);

            albumsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(MainActivity.this, AlbumActivity.class);
                    Bundle b = new Bundle();
                    b.putString("id", ((Album) parent.getItemAtPosition(position)).getId());
                    intent.putExtras(b);
                    startActivity(intent);
                }
            });

        }
    }
}
