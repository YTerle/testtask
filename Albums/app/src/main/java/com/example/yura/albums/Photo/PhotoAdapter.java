package com.example.yura.albums.Photo;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yura.albums.R;

import java.util.List;

public class PhotoAdapter extends ArrayAdapter<Photo> {

    public PhotoAdapter(Context context, List<Photo> photos) {
        super(context, R.layout.photos_list_design, photos);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.photos_list_design, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.photoThumbnailImageView = (ImageView) convertView.findViewById(R.id.thumbnailPhoto);
            viewHolder.photoTitleTextView = (TextView) convertView.findViewById(R.id.titlePhoto);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Photo photo = getItem(position);

        if (photo != null){
            viewHolder.photoThumbnailImageView.setImageBitmap(photo.getThumbnail());
            Log.i("info", "thumbnail setted to ImageView " + position);
            viewHolder.photoTitleTextView.setText(photo.getTitle());
        }
        return convertView;
    }

    private static class ViewHolder{
        private ImageView photoThumbnailImageView;
        private TextView photoTitleTextView;
    }
}