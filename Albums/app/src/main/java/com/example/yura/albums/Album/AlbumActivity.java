package com.example.yura.albums.Album;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.yura.albums.Photo.Photo;
import com.example.yura.albums.Photo.PhotoActivity;
import com.example.yura.albums.Photo.PhotoAdapter;
import com.example.yura.albums.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AlbumActivity extends AppCompatActivity {

    private ListView photosListView;

    PhotoAdapter photosListViewAdapter;

    private String photosUrl;
    private List<Photo> photos;

    private static final String TAG_ID = "id";
    private static final String TAG_TITLE = "title";
    private static final String TAG_URL = "url";
    private static final String TAG_THUMBNAILURL = "thumbnailUrl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        Bundle b = getIntent().getExtras();
        photosUrl = "http://jsonplaceholder.typicode.com/photos?albumId=" + (String)b.get("id");

        photosListView = (ListView) findViewById(R.id.listPhotos);
        photos = new ArrayList<>();

        new PhotosDownloader().execute();
    }

    private class ThumbnailDownloader extends AsyncTask<Void, Void, Void> {

        private int position;
        public void setPosition(int position) {
            this.position = position;
        }

        @Override
        protected Void doInBackground(Void... urls) {
            try {
                Photo photo = photos.get(position);
                Bitmap image = Picasso.with(getBaseContext()).load(photo.getThumbnailUrl()).get();
                photo.setThumbnail(image);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            photosListViewAdapter.notifyDataSetChanged();
        }
    }

    private class PhotosDownloader extends AsyncTask<Void, Void, Void> {

        private String retrieveJSONString(String url){
            String JSONOutputStr = null;
            try {
                URL albumsUrl = new URL(url);
                HttpURLConnection AlbumsConnection = (HttpURLConnection) albumsUrl.openConnection();
                AlbumsConnection.setRequestMethod("GET");
                AlbumsConnection.connect();

                InputStream in = AlbumsConnection.getInputStream();
                BufferedReader streamReader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String lineOfJSONCode;
                while ((lineOfJSONCode = streamReader.readLine()) != null)
                    responseStrBuilder.append(lineOfJSONCode);

                JSONOutputStr = responseStrBuilder.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return JSONOutputStr;
        }

        @Override
        protected Void doInBackground(Void... params) {

            String JSONOutputString = retrieveJSONString(photosUrl);

            if (JSONOutputString != null) {
                try {

                    JSONArray jsonArray = new JSONArray(JSONOutputString);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String id = jsonObject.getString(TAG_ID);
                        String title = jsonObject.getString(TAG_TITLE);
                        String url = jsonObject.getString(TAG_URL);
                        String thumbnailUrl = jsonObject.getString(TAG_THUMBNAILURL);

                        String code = thumbnailUrl.substring(url.lastIndexOf('/') + 1);
                        String newThumbnailUrl =
                                "https://placeholdit.imgix.net/~text?txtsize=14&bg=" + code + "&txt=150%C3%97150&w=150&h=150";

                        photos.add(new Photo(id, title, url, newThumbnailUrl));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("parser", "Couldn't get any data from the url");
            }
            return null;
        }

        private void startThumbnailsDownloading(){
            for (int i = 0; i < photos.size(); i++) {
                ThumbnailDownloader td = new ThumbnailDownloader();
                td.setPosition(i);
                td.execute();
            }
        }

        @Override
        protected void onPostExecute(Void result){
            super.onPostExecute(result);

            photosListViewAdapter = new PhotoAdapter(getApplicationContext(), photos);
            photosListView.setAdapter(photosListViewAdapter);

            startThumbnailsDownloading();

            photosListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    Intent intent = new Intent(AlbumActivity.this, PhotoActivity.class);
                    Bundle b = new Bundle();
                    b.putString("url", ((Photo) parent.getItemAtPosition(position)).getUrl());
                    intent.putExtras(b);
                    startActivity(intent);
                }
            });

        }
    }
}