package com.example.yura.albums.Album;

public class Album {

    private String id;
    private String title;

    public Album(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId(){
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }
}
