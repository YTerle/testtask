package com.example.yura.albums.Photo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.yura.albums.R;
import com.squareup.picasso.Picasso;

public class PhotoActivity extends AppCompatActivity {

    private ImageView imageView;
    private String photoUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        imageView = (ImageView)findViewById(R.id.photo);

        Bundle b = getIntent().getExtras();
        photoUrl = (String)b.get("url");

        String code = photoUrl.substring(photoUrl.lastIndexOf('/') + 1);
        String newPhotoUrl = "https://placeholdit.imgix.net/~text?txtsize=56&bg=" + code + "&txt=600%C3%97600&w=600&h=600";

        Picasso.with(this).load(newPhotoUrl).fit().into(imageView);


    }
}