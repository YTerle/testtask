package com.example.yura.albums;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class InternetAccessChecker {

    private boolean isInternetConnected = false;
    private Context context;

    public InternetAccessChecker(Context context) {
        this.context = context;
    }

    public void trackInternetConnection(){
        isInternetConnected = isOnline();
        if (isInternetConnected == false)
            checkEveryNseconds(2);
    }

    private void checkEveryNseconds(int N) {
       new Timer().scheduleAtFixedRate(new TimerTask() {
           @Override
           public void run() {
               isInternetConnected = isOnline();
               if (isInternetConnected == false)
                   ((MainActivity)context).runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           notifyThatNotConnected();
                       }
                   });
               if (isInternetConnected == true){
                   this.cancel();
                   ((MainActivity)context).runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           reloadActivity();
                       }
                   });
               }
           }
       }, 0, N * 1000);
    }

    private void reloadActivity() {
        ((MainActivity)context).recreate();
    }

    private void notifyThatNotConnected() {
        Toast.makeText(context,"No internet connection", Toast.LENGTH_SHORT).show();
    }

    private boolean isOnline(){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}