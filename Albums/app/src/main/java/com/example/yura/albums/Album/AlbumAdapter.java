package com.example.yura.albums.Album;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.yura.albums.R;

import java.util.List;

public class AlbumAdapter extends ArrayAdapter<Album>{

    public AlbumAdapter(Context context, List<Album> albums) {
        super(context, R.layout.albums_list_design,albums);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.albums_list_design, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.albumTitleTextView = (TextView)convertView.findViewById(R.id.albumTitleTextView);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder)convertView.getTag();
        }

        Album album = getItem(position);

        viewHolder.albumTitleTextView.setText(album.getTitle());

        return convertView;
    }

    private static class ViewHolder{
        private TextView albumTitleTextView;
    }
}
